#Planning Your Intermediate Engineering Year

**Author: Hayley van Waas, University of Canterbury, September 2015**

This is an interactive webpage designed to help students in selecting which papers to take for first year engineering at the University of Canterbury.

### To edit the table rules
The page works according to rules specified in dictionaries in script.js. One dictionary specifies subjects required for each engineering discipline, and the other specifies which subjects are available in each semester. The last dictionary specifies which subjects are common to all engineering types. To change the rules, simply edit these dictionaries.

The initial rules were taken directly from the Intermediate Courses Advisor spreadsheet.

### Adapting this program
This webpage is designed specifically for students who have been through the NCEA school system. To adapt the webpage for different systems, the function adjustRules should be rewritten.

### Required Files
The webpage uses
  - jQuery
  - Code shared under the [Universal Licence](https://creativecommons.org/publicdomain/zero/1.0/)
  - normalize.css as used under the licence specified in the [LICENSE- normalize.md](LICENSE - normalize.md) file
  - trashcan icon used under the MIT Licence